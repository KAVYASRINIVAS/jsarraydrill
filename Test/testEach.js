const testEach = require('../each.js');

const items = [1, 2, 3, 4, 5, 5];
console.log(testEach(items, function areaOfSquare(ele, i, elementsArray) {
    let res = ele * ele;
    //console.log(i,res,elementsArray);
    return res;
}));

console.log(testEach(function areaOfSquare(ele, i, elementsArray) {
    let res = ele * ele;
    //console.log(i,res,elementsArray);
    return res;
}));

console.log(testEach(items));

console.log(testEach());
