const filterTest=require('../filter.js');

const items = [1, 10, 3, 4, 5, 25];
const output = filterTest(items, function divisibleByFive(ele) {
    return (ele % 5 === 0) ? true : false;
});

console.log(output);