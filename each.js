function each(elements, cb) {
    let result = [];
    if (!elements || !cb || elements.length === 0) {
        return result;
    } else {
        for (let index = 0; index < elements.length; index++) {
            result.push(cb(elements[index], index, elements));
        }
        return result;
    }
}

module.exports = each;