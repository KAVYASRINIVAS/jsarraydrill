function filter(elements, cb) {
    if (!elements || !cb) {
        return [];
    } else {
        let result = [];
        for (let index = 0; index < elements.length; index++) {
            const flag = cb(elements[index]);
            if (flag === true) {
                result.push(elements[index]);
            }
        }
        return result;

    }
}

module.exports = filter;