function find(elements, cb) {
    if (!elements || !cb) {
        return [];
    } else {
        let result = [];
        for (let index = 0; index < elements.length; index++) {
            const flag = cb(elements[index]);
            if (flag === true) {
                result.push(elements[index]);
            }else{
                result.push(undefined);
            }
        }
        return result;

    }
}

module.exports = find;
