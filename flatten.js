let eleArray = [];

function flatten(element) {
    if (!element) {
        return [];
    } else {
        for (let i = 0; i < element.length; i++) {
            if (typeof element[i] === 'string' || typeof element[i] === 'number' || typeof element[i] === 'boolean') {
                eleArray.push(element[i]);
            } else {
                flatten(element[i]);
            }
        }
        return eleArray;
    }
}

module.exports = flatten;

