function reduce(elements,cb,startingValue){
    if(!elements || !cb){
        return [];
    }
    if(!startingValue){
        startingValue=elements[0];
    }
    for(let i=0;i<elements.length;i++){
        startingValue=cb(startingValue,elements[i]);
    }
    return startingValue;
}


const items = [10, 25, 64, 121, 15, 5];

const result=reduce(items,function sumOfArray(sum,ele){
    return sum+ele;
},1);
console.log(result);

const result1=reduce(items,function sumOfArray(sum,ele){
    return sum+ele;
});
console.log(result1);

const result2=reduce(function sumOfArray(sum,ele){
    return sum+ele;
});
console.log(result2);

const result3=reduce();
console.log(result3);


